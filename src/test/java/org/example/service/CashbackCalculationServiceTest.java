package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CashbackCalculationServiceTest {
    @Test
    void shouldCalculateOverHundred() {
        CashbackCalculationService service = new CashbackCalculationService();
        int purchase = 400_00;
        int percentage = 100_00;
        int expected = 4;

        int actual = service.calculate(purchase, percentage);

        assertEquals(expected, actual);
    }

    @Test
    void shouldCalculateUnderHundred() {
        CashbackCalculationService service = new CashbackCalculationService();
        int purchase = 90_00;
        int percentage = 100_00;
        int expected = 0;

        int actual = service.calculate(purchase, percentage);

        assertEquals(expected, actual);
    }

    @Test
    void shouldCalculateNotRound() {
        CashbackCalculationService service = new CashbackCalculationService();
        int purchase = 5699_99;
        int percentage = 100_00;
        int expected = 56;

        int actual = service.calculate(purchase, percentage);

        assertEquals(expected, actual);
    }

}

